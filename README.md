# Introduction - Présentation du sujet

![Image presentation](./Presentation.png).

De nos jours, dans un monde où le numérique est omniprésent, les cyberattaques sont de plus en plus présentes.
Le domaine de la cybersécurité est alors en croissante exponentielle.

Un des métiers liés à ce domaine est le métier de Pentester avec un seul objectif : Renforcer la sécurité d'une infrastructure réseau après avoir lancé différentes attaques d'intrusion afin de démontrer les faiblesses du Système d'Information.

Dans ce projet directement en lien avec la cybersécutité, nous avons ainsi développé une application de pentesting afin de lancer diférentes attaques et de tester la robustesse d'un réseau.

Les différentes fonctions de notre application sont présentées et décrites ci-dessous.

Les fonctions disponibles :
- Phishing
- DDoS
- MITM
- STP usurpation
- ARP Poisoning

# Fonctionnalité STP usurpation

Cette fonction permet, une fois branchée/connecté sur un switch, de se faire passer pour un switch ayant une adresse mac la moins elevée possible afin d'être le switch racine ainsi donc intercepter le trafic du reseau voulu.

![alt text](https://slideplayer.fr/slide/11936473/67/images/31/Attaque+manipulation+du+STP.jpg)

# Fonctionnalité DDOS 

![alt text](https://www.it-connect.fr/wp-content-itc/uploads/2022/08/Schema-attaque-DDoS-800x474.png)


# Fonctionnalité MITM

Cette fonctionnalité permet de détourner la machine cible vers une fausse passerelle (notre machine) afin de capturer tout son trafic.

![alt text](https://www.hsc.fr/wp-content/uploads/2022/12/attaque-man-in-the-middle.webp)

# Fonctionnalité ARP Poisoning

![Arp poisoning](./ARP.png)

Cette attaque permet d'exploiter les faiblesses de l'ARP pour corrompre la table MAC des appareils sur le réseau afin d'en capturer le trafic. Sur la fonctionnalité, il vous faut entrer les adresses ip cibles.

# Fonctionnalité Phishing

![Img Phishing](./phishing.jpg)

Cette fonctionnalitée permet de créer facilement à partir de templates pre-existant un faux site déjà pre-configuré afin que le client cible entre ses coordonnées bancaires ou bien ses identifiants du site voulu.
Pour cela, il suffit d'entrer votre adresse mail où vous voulez recevoir les données entrées des clients ainsi que copier/coller le site généré dans votre dossier hebergeant le serveur web et ses pages.

# Auteurs 

Aleksandra DOBRIC et Lucas Piveteau